import { AngularMeetupPage } from './app.po';

describe('angular-meetup App', () => {
  let page: AngularMeetupPage;

  beforeEach(() => {
    page = new AngularMeetupPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to ngm!');
  });
});

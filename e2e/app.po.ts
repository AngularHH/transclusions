import { browser, by, element } from 'protractor';

export class AngularMeetupPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('ngm-root h1')).getText();
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { CardGridComponent } from './card-grid/card-grid.component';
import { CardConfigComponent } from './card-config/card-config.component';
import { CardViewComponent } from './card-view/card-view.component';
import { TemplateWrapperDirective } from './template-wrapper.directive';
import { TransclusionTestComponent } from './transclusion-test/transclusion-test.component';

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    CardGridComponent,
    CardConfigComponent,
    CardViewComponent,
    TemplateWrapperDirective,
    TransclusionTestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

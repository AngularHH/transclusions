import { TemplateWrapperDirective } from './../template-wrapper.directive';
import { Component, AfterContentInit, Input, QueryList, ViewChildren, TemplateRef, ContentChildren } from '@angular/core';

@Component({
  selector: 'ngm-card-config',
  templateUrl: './card-config.component.html',
  styleUrls: ['./card-config.component.css']
})
export class CardConfigComponent implements AfterContentInit {

  @Input() header: string;
  @Input() footer: string;
  @Input() title: string;
  @Input() body: string;
  @ContentChildren(TemplateWrapperDirective) templates: QueryList<TemplateWrapperDirective>;
  private initialized = false;

  public get bodyTemplate(): TemplateRef<any> {
    if (!this.templates)
      return undefined;
    let temp = this.templates.find(t => t.name === "body");
    console.log(temp);
    if (!temp)
      return undefined;
    return temp.template;
  }


  constructor() { }

  ngAfterContentInit() {
    this.initialized = true;
    // setTimeout(() => this.initialized = true, 1000);
  }

}

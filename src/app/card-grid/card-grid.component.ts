import { CardConfigComponent } from './../card-config/card-config.component';
import { Component, AfterContentInit, ViewChildren, QueryList, ContentChildren } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'ngm-card-grid',
  templateUrl: './card-grid.component.html',
  styleUrls: ['./card-grid.component.css']
})
export class CardGridComponent implements AfterContentInit {

  private initialized = false;
  @ContentChildren(CardConfigComponent) cardConfigs: QueryList<CardConfigComponent>;
  public get configs(): CardConfigComponent[] {
    return this.initialized && this.cardConfigs ? this.cardConfigs.toArray() : [];
  }
  private contextObject: any = { text: 'Context defined in card grid' };
  public context: any = { $implicit: this.contextObject, anotherVar: 'im an alternative context var' };

  constructor() { }

  ngAfterContentInit() {
    this.initialized = true;
  }

}

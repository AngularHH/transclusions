import { TemplateWrapperDirective } from './../template-wrapper.directive';
import { Component, OnInit, ContentChild } from '@angular/core';

@Component({
  selector: 'ngm-card-view',
  templateUrl: './card-view.component.html',
  styleUrls: ['./card-view.component.css']
})
export class CardViewComponent implements OnInit {

  @ContentChild(TemplateWrapperDirective) template: TemplateWrapperDirective;
  public data: any = { text: 'I\'m text from the data object' };

  constructor() { }

  ngOnInit() {
  }

}

import { TransclusionTestComponent } from './../transclusion-test/transclusion-test.component';
import { Component, OnInit, ContentChild, AfterContentInit } from '@angular/core';

@Component({
  selector: 'ngm-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements AfterContentInit {

  constructor() { }

  ngAfterContentInit() {
  }

}

import { Directive, TemplateRef, ViewChild, Input, ContentChild } from '@angular/core';

@Directive({
  selector: '[ngmTemplate]'
})
export class TemplateWrapperDirective {

  @ContentChild(TemplateRef) template: TemplateRef<any>;
  @Input('ngmTemplate') public name: string;

  constructor() { }

}
